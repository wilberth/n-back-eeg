#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import numpy as np
import copy
import sys
import time
from psychopy import core, visual, event, gui
from rusocsci import buttonbox
import shuffle

 
## Setup Section
# constants
stimulus_characters   = ('B', 'C', 'D', 'E', 'G', 'J', 'P', 'T', 'V', 'W')
duration              = 30*60    # s, duration of test 1
error_threshold       = 10       # error per block, when to stop 3-back tast
text_height           = 40       # pixels
n_target              = 25       # number of targets per block
stimulus_display_time = 0.500    # s
response_time         = 2.000    # s
isi_time              = (2.50, 2.75, 3.00) # s, time between stimulus onsets
block_length          = 10 # number of times all stimuli are repeated in each block
rating_time           = 10 # s, one rating scale 
questionnaire_time    = 30 # s, whole questionnaire
include_test          = (True, True, True) # which tests to include (practice, 1, 2)


# initialization
win = visual.Window([640, 480], monitor="testMonitor", units="pix")
intro = visual.TextStim(win, text="Hello,\nWelcome to this experiment.\n\nPress any key to continue.")
test_1_intro = visual.TextStim(win, text="Now for real.")
test_2_intro = visual.TextStim(win, text="Let's see if you can doe it 3-back too.")
stimuli = [visual.TextStim(win, text=x, height=text_height) for x in stimulus_characters]
feedback = visual.TextStim(win, text="", pos=(150, 120))
feedback.setAutoDraw(True)
stimulus_indices = np.array(range(len(stimuli))*block_length) # ten times the number of stimuli
bb = buttonbox.Buttonbox()
t_test = 0 # time at start of task, reference time for tso and tr

# interactive initialization
# request participant number
info = {'participant': 0}
dictDlg = gui.DlgFromDict(dictionary=info)
participant = info['participant']

# open files
name = "{:05d}_{:}".format(participant, time.strftime('%Y-%m-%d_%H.%M.%S'))
trial_file = open("data/{}_t.dat".format(name), "wb")
trial_file.write("test\tblock\tTSO\tTR\ttarget\tcor_res\tinc_res\tinc_rej\tcor_rej\tRT\n")
block_file = open("data/{}_b.dat".format(name), "wb")
block_file.write("fatigue\teffort\n")

## Experiment Section
# functions 
def block(win, stimulus_indices, n_back=2, n_incorrect=0, test_name="", i_block=0):
	" Show block of len(stimulus_indices) trials "
	np.random.shuffle(stimulus_indices) # shuffle trials for this block
	stimulus_indices2 = copy.deepcopy(stimulus_indices)
	shuffle.shuffle_n_back(stimulus_indices2, n_back, n_target) # with n-back
	feedback.text = "incorrect:   0"

	for i_trial in range(len(stimulus_indices)):
		stimulus = stimuli[stimulus_indices2[i_trial]]
			
		# send marker for eeg synchronisation
		bb.sendMarker([True])
		
		# show stimulus and record response
		stimulus.draw()
		win.flip()
		t_presentation = core.getTime()
		keys = []
		while core.getTime() < t_presentation + response_time and not keys:
			if core.getTime() < t_presentation + stimulus_display_time:
				stimulus.draw()
			win.flip()
			keys = event.getKeys()
		t_response = core.getTime()
		
		tso = t_presentation - t_test
		if keys:
			rt = t_response - t_presentation
			tr = t_response - t_test
		else:
			rt = -1
			tr = -1
			
		# allow abort
		#if len(keys) and keys[0] == 'escape':
			#exit()
		
		# feedback
		correct_response, incorrect_response, incorrect_rejection, correct_rejection = (0,0,0,0)
		target = i_trial>=n_back and stimulus_indices2[i_trial]==stimulus_indices2[i_trial-n_back] # should a key be pressed?
		if target:
			if keys:
				correct_response = 1
			else:
				incorrect_response = 1
				n_incorrect += 1
		else:
			if keys:
				incorrect_rejection = 1
				n_incorrect += 1
			else:
				correct_rejection = 1
		feedback.text="incorrect: {:3d}".format(n_incorrect)
		feedback.draw()
		win.flip()

		# inter trial interval
		bb.sendMarker([False]) # reset marker
		dt = isi_time[int(np.random.random()*len(isi_time))] # waiting time since onset of stimulus
		core.wait(dt + t_presentation - core.getTime())
		trial_file.write("{:s}\t{:d}\t{:.3f}\t{:.3f}\t{:d}\t{:d}\t{:d}\t{:d}\t{:d}\t{:.3f}\n".format(
			test_name, i_block+1, tso, tr, target, 
			correct_response, incorrect_response, incorrect_rejection, correct_rejection, rt,
			))

	return n_incorrect

def questionnaire(win):
	" show questionnaire "
	t_questionnaire = core.getTime() # time of presentation of first scale
	response = []
	for text in ('how fatigued do you feel at this moment?', 'how effortful was the task?'):
		t_rating = core.getTime() # time of presentation scale (roughly)
		text_stim = visual.TextStim(win, text=text, pos=(0.0, 0.5))
		rating_scale = visual.RatingScale(win, low=0, high=100)
		while rating_scale.noResponse and core.getTime() < t_rating + rating_time:
			text_stim.draw()
			rating_scale.draw()
			win.flip()
		response.append((rating_scale.getRating(), -1)[rating_scale.noResponse])
	block_file.write("{:d}\t{:d}\n".format(response[0], response[1]))
	text_stim = visual.TextStim(win, text='the next block of trials will automatically start in a few seconds...')
	while core.getTime() < t_questionnaire + questionnaire_time:
		text_stim.draw()
		win.flip()

## experiment script
# introduction
if include_test[0]:
	intro.draw()
	win.flip()
	event.waitKeys()

	## 2-back practise
	t0 = core.getTime()
	n_incorrect_cum = 0 # total incorrect in all blocks
	t_test = core.getTime()
	block(win, stimulus_indices, 2, test_name="pract", i_block=0)
	questionnaire(win)
	
## 2-back test 1
if include_test[1]:
	t0 = core.getTime()
	n_incorrect_cum = 0 # total incorrect in all blocks
	i_block = 0

	# ask for desired duration
	info = {'test duration (min)': duration/60}
	dictDlg = gui.DlgFromDict(dictionary=info)
	duration = 60.0*info['test duration (min)']

	test_1_intro.draw()
	win.flip()
	event.waitKeys()

	t_test = core.getTime()
	while core.getTime() < t_test + duration:
		n_incorrect_cum += block(win, stimulus_indices, 2, n_incorrect_cum, test_name="test 1", i_block=i_block)
		questionnaire(win)
		i_block+=1
	
## 3-back, test 2
if include_test[2]:
	n_incorrect = n_incorrect_cum = 0
	i_block = 0

	# ask for desired duration
	info = {'max error per block': error_threshold}
	dictDlg = gui.DlgFromDict(dictionary=info)
	error_threshold = info['max error per block']

	test_2_intro.draw()
	win.flip()
	event.waitKeys()

	t_test = core.getTime()
	while n_incorrect < error_threshold:
		n_incorrect = block(win, stimulus_indices, 3, n_incorrect_cum, test_name="test 2", i_block=i_block)
		n_incorrect_cum += n_incorrect
		questionnaire(win)
		i_block+=1
	
## Closing Section
trial_file.close()
block_file.close()
win.close()
core.quit()
