#!/usr/bin/env python
""" Some shuffle functions commonly used in psychology experiments """
import numpy as np

def shuffle(a):
	""" return shuffled copy of 1D array """
	return np.random.permutation(a)

def draw(a, n, repeat = True):
	""" draw n values from array a. 
	If repeat=False values will not start repeating before all values have been used. 
	"""
	a = np.array(a)
	if repeat:
		return a[np.random.randint(0, len(a),(n,))]
	
	b = np.empty(n)
	for i in range(n//len(a)):
		b[len(a)*i:(len(a)+1)*i] = numpy.random.permutation(a) # all elements of a shuffled
	b[-(n%len(a)):] = np.random.permutation(a)[0:n%len(a)]
	[np.random.randint(0, len(a),(n%len(a),))] # remaining drawn  to fill b
	return b
	
def count_n_back(a, n_back=2, l=None):
	count = 0
	for i in range(n_back, len(a)):
		if a[i] == a[i-n_back]:
			count += 1
			if l is not None: 
				l.append(i)
	return count

def shuffle_n_back(a, n_back, n_occurence):
	""" in place shuffle until there are exactly n_occurence n-backs"""
	l = [] # list of n-backs
	while count_n_back(a, n_back, l) < n_occurence:
		#print("l: {}".format(l))
		l0 = set(l) | set(np.array(l)-n_back) # leave these in place
		l1 = set(range(n_back, len(a))) - l0  # choose from these
		
		# choose two different elements from this set
		i = np.random.choice(list(l1), 2, False)
		
		# swap these two
		a[i[0]], a[i[1]] = a[i[1]], a[i[0]]
		l = []
	return a
		

def draw_n_back(a, n, n_back, n_occurence):
	""" draw n values from array. 
	there will be n_occurence places where the element n_back is identical
	"""
	a = np.array(range(n))
	shuffle_n_back(a, n_back, n_occurence)
	return a
	
	
if __name__ == "__main__":
	a = np.random.randint(0, 10, 100)
	print(a)
	a = shuffle(a)
	print(a)
	print(count_n_back(a))
	print(shuffle_n_back(a, 2, 25))
	print(count_n_back(a))
