N-Back task (2-back/3-back)
- letters B, C, D, E, G, J, P, T, V, and W (white against black background; presented in the center of the screen; font size = 40).
- Stimuli presented in random order with a target rate (i.e. stimulus is same as stimulus n-back in the series) of 25%.
- Stimulus display time: 500 ms
- Response window (starting upon stimulus onset): 2000 ms
- Stimuli presented in blocks of 100 trials
- Interstimulus interval: 2.50, 2.75 or 3.00 sec (randomized within blocks)
- NB: for EEG synchronization à send event marker upon stimulus onset

Questionnaire
- presented after each block
- slider scales (0-100)
  o Q1: how fatigued do you feel at this moment? (labels: not at all – extremely)
  o Q2: how effortful was the task? (labels: not at all – extremely)
  o response window: 10 seconds (starting upon display)
- after response to second question show screen: “the next block of trials will automatically start in a few seconds…”
- new block of trials starts at fixed time (i.e., 25 sec after presentation of Q1)

Feedback
- error count (false alarms + false negatives; add across blocks)
- presented in corner of the screen

Procedure
- Practice (2-back)
  o 1 block

- Test 1 (2-back)
  o X blocks (set manually before test starts) (if I’m correct, 1 block of 100 trials [incl. questionnaire] should take 100*2.75 + 25 = 300 seconds [5 minutes])

- Test 2 (3-back)
  o X blocks (until error threshold is reached; e.g., X errors per block; set manually before test starts)
  o always complete last block

Output-file
- trial-level (N-back task)
  o test (practice, test1, test2)
  o block (1, 2, 3, 4, …)
  o time stimulus onset (TSO; time since first trial of block 1 of current test)
  o time response (TR; time since first trial of block 1 of current test)
  o target / non-target (1/0)
  o correct response (1/0) “true positive”
  o incorrect response (1/0) “false alarm”
  o incorrect rejection (1/0) “false negative”
  o correct rejection (1/0) “true negative”
  o reaction time (RT; delay stimulus –> response)

- block-level (Questionnaire)
  o fatigue score (0-100)
  o effort score (0-100)